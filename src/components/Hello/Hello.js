import React, { useEffect, useState } from 'react';
import './Hello.css';
import song from './audio.wav'
import './style.css'
import data from './data.json'
import ReactDOM from 'react-dom';

function Hello() {
    const [audio, setAudio] = useState(new Audio(song));
    const [idx, setIdx] = useState(0);
    const [play, setPlay] = useState(false);
    const [len, setLen] = useState();
    const [speed, setSpeed] = useState(1);

    function toggleSong() {
        if (!play) {
            audio.play();
            setPlay(true);
            setLen(data.word_timings[0].length);
        }
        else {
            audio.pause();
            setPlay(false);
        }
    }

    useEffect(() => {
        if (play && idx < len) {

            let objArr = data.word_timings[0];
            console.log(objArr[idx])
            let endString = objArr[idx].endTime.substring(0, objArr[idx].endTime.length - 1);
            let startString = objArr[idx].startTime.substring(0, objArr[idx].startTime.length - 1)
            let diff = parseFloat(endString) * 1000 - parseFloat(startString) * 1000;
            diff = diff / speed
            console.log(diff);
            let element = document.getElementById(objArr[idx].startTime)
            ReactDOM.findDOMNode(element).classList.add("highlight")
            // let diff = parseFloat(objArr[idx].endTime)*100 - parseFloat(objArr[idx].startTime)*100
            let interval = setTimeout(() => {
                setIdx(idx + 1);
            }, diff);

            return () => {
                clearInterval(interval);
                ReactDOM.findDOMNode(element).classList.remove("highlight")
            };
        }
    });

    function selectVal(e) {
        setSpeed(e.target.value);
        audio.playbackRate = e.target.value;
    }

    function moveBack() {
        audio.currentTime -= 10;
    }

    function moveForward() {
        console.log(audio.currentTime)
        audio.currentTime += 10.0;
        console.log(audio.currentTime)
    }

    return (
        <div>
            <header>
                <div className="nav-bar">
                    <div className="music-comp">
                        <img src="replay-10.png" className="replay" onClick={moveBack} />
                        {!play ? <img src="play.png" onClick={toggleSong} /> : <img src="pause.png" onClick={toggleSong} />}
                        <img src="forward-10.png" className="fwd" onClick={moveForward} />
                        <div>
                            <select className="speed" onChange={selectVal} >
                                <option value=".50" key="0.5">0.5x</option>
                                <option value="0.75" key="1.75">0.75x</option>
                                <option value="1" key="1" selected>1.0x</option>
                                <option value="1.5" key="1.5">1.5x</option>
                                <option value="2" key="2">2.0x</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="wave-form">
                    <div className="seek-div">

                    </div>
                </div>

                <div className="outer">
                    <div className="duration">
                        <div className="current-time">{(audio.currentTime+'').substring(0,4)}/{audio.duration}</div>
                    </div>
                    <div className="name-and-seek-bar">
                        <div className="name">
                            <div className="me">me</div>
                            <div className="you">you</div>
                        </div>
                        <div className="seek-bar-div">
                            <div className="purple">purple</div>
                            <input type="range" value={(audio.currentTime/audio.duration)*100} max='100' className="seek-bar" />
                            <div className="violet">violet</div>
                        </div>
                    </div>
                </div>
            </header>

            <div className="conv-text">
                <div className="me">
                    <div className="time">01:00</div>
                    <div className="vertical-bar">
                        <div className="text">{data.word_timings[0].map((obj, idx) => <span id={obj.startTime} className="word">{obj.word}</span>)}
                        </div>
                    </div>
                </div>
                <br />
                <div className="other">
                    <div className="time">01:00</div>
                    <div className="vertical-bar">

                        <div className="text">{data.transcript_text[1]}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Hello;
