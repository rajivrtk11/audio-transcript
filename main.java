import java.util.*;

public class main{
    public static void main(String[] args){
        Scanner scn  = new Scanner(System.in);
        // int t = scn.nextInt();

        // while(t-- > 0){
            int n = scn.nextInt();           
            int m = scn.nextInt();

            // int[] arr = new int[m];
            // for(int i = 0; i < m; i++){
            //     arr[i] = scn.nextInt();
            // }            

            int curr = 1;
            long ans = 0;
            for(int i = 0; i < m; i++){
                int val = scn.nextInt();
                if(curr <= val){
                    ans += val - curr;
                }
                else{
                    ans += n - curr + val;
                }
                curr = val;
            }

            System.out.println(ans);
        // }
    }
}